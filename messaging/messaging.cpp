#include "messaging.h"

#include <iostream>
#include <string>
#include <cstdlib>
#include <set>
#include <map>
#include <string>
#include <cctype>

using namespace std;

struct server {
    map< struct receiver *, set<set<string> > > tags;
};

/* return 0 on failure */
struct server * server_new() {
#if 0
    struct server * s = (struct server*)calloc(1, sizeof(struct server));
#else
    struct server * s = new server;
#endif
    if (!s) {
        return 0;
    }
    return s;
}

void server_delete(struct server * s) {
#if 0
    free(s);
#else
    delete (s);
#endif
}

/* return 0 on failure */
int add_interest(struct server * srv, struct receiver * r, const char * interest) {
    string tag;
    string interests(interest);
    set<string> tags;
    bool begin = true;
    for (auto&c : interests) {
        if ( isalpha(c) || (begin && (c == '#' || c == '@'))) {
            tag.push_back(c);
            begin = false;
        } else {
            if (!tag.empty()) {
                tags.insert(tag);
                tag.clear();
            }
            begin = true;
        }
    }
    if (!tag.empty()) {
        tags.insert(tag);
    }

    srv->tags[r].insert(tags);

    return 1;
}

void remove_interest(struct server * srv, struct receiver * r, const char * interest) {
    string tag;
    string interests(interest);
    set<string> tags;
    bool begin = true;
    for (auto&c : interests) {
        if ( isalpha(c) || (begin && (c == '#' || c == '@'))) {
            tag.push_back(c);
            begin = false;
        } else {
            if (!tag.empty()) {
                tags.insert(tag);
                tag.clear();
                begin = true;
            }
        }
    }
    if (!tag.empty()) {
        tags.insert(tag);
    }

    srv->tags[r].erase(tags);
}

void clear_receiver(struct server * srv, struct receiver * r) {
    srv->tags.erase(r);
}

void clear_all(struct server * srv) {
    srv->tags.clear();
}

void send(const struct server * srv, const char * message) {
    string tag;
    string interests(message);
    set<string> tags;
    bool begin = true;
    for (auto&c : interests) {
        if ( isalpha(c) || (begin && (c == '#' || c == '@'))) {
            tag.push_back(c);
            begin = false;
        } else {
            if (tag.size() > 0) {
                tags.insert(tag);
                tag.clear();
                begin = true;
            }
        }
    }
    if (!tag.empty()) {
        tags.insert(tag);
    }

    for (auto& r : srv->tags) {
        for (auto& rcv_tags : srv->tags.at(r.first)) {
#if 0
            if (rcv_tags == tags) { //iterate over set of tags
                r.first->deliver(r.first, message);

            }
#endif
            bool not_found = 0;
            auto tag = rcv_tags.begin();
            while (tag != rcv_tags.end() && !not_found) {
                auto itr = tags.find(*tag);
                if (itr == tags.end()) {
                    not_found = 1;

                }
                ++tag;
            }

            if (!not_found) {
                r.first->deliver(r.first, message);
                break;
            }

        }
    }
}
