#include "s_table.h"

#include <cstdio>
#include <stddef.h>
#include <stdio.h>
#include <set>
#include <map>
#include <string>
#include <iostream>

using namespace std;

struct s_table {
    set<string> records;
};

/* Create and return a new s_table object.  Return 0 on failure. */
struct s_table * s_table_new() {
    try {
        return new s_table;
    } catch (exception& e) {
        return 0;
    }

}

/* Destroy the given s_table object and release all memory allocated by it. */
void s_table_delete(struct s_table * t) {
    delete(t);
}

void s_table_clear(struct s_table * t) {
    t->records.clear();
}

/* Add a record given by the give string with the given coordinates and description.
 * Return 0 on failure, 1 on success. */
int s_table_add_record(struct s_table * t, const char * begin, const char * end) {
    return t->records.insert(string(begin)).second;
}

/* Remove a record with the given coordinates and description.
 * Return 0 if not found, 1 if found (and removed). */
int s_table_remove_record(struct s_table * t, const char * begin, const char * end) {
    return t->records.erase(string(begin));
}

/* Remove all the records that are selected by the given callback
 * function. A record is selected if the selector callback returns an
 * integer value that compares TRUE (i.e., != 0).
 *
 * Return the number of records that were removed. */
size_t s_table_remove_records(struct s_table * t, feature_extractor selector_callback) {
    size_t counter = 0;
    auto itr = t->records.begin();
    while (itr != t->records.end()) {
        const char * begin = itr->c_str();
        const char * end = begin + itr->size();
        if (selector_callback(begin, end)){
            itr = t->records.erase(itr);
            ++counter;
            continue;
        }
        ++itr;
    }
    return counter;
}

/* Find a record with the maximal feature extracted by the given
 * callback function.  Copies the corresponding record in the given
 * buffer, defined by the record char pointer and the given max buffer
 * length.  Never copies more than record_max_len characters.
 *
 * Return the number of characters copied in the record buffer, or 0 
 * if there are no records in the table. */
size_t s_table_max_feature(struct s_table * t, feature_extractor feature_callback,
			   char * record, size_t record_max_len) {

    string max_str;
    int max = 0;

    for (auto& r : t->records) {
        const char * begin  = r.c_str();
        const char * end = begin + r.size();
        int res = feature_callback(begin, end);
        if (res >= max) {
            max = res;
            max_str = r;
        }
    }

    int counter = 0;
    for (auto& ch : max_str) {
        record[counter] = ch;
        ++counter;
    }
    return counter;
}

/* Print the table on the given FILE stream, one record per line, with
 * the records sorted in increasing order of the feature extracted by
 * the given callback function. */
void s_table_print_sorted(struct s_table * t, FILE * f,
			  feature_extractor feature_callback) {

    map<int, string> sorted_records;
    for (auto& r : t->records) {
        const char * begin  = r.c_str();
        const char * end = begin + r.size();
        int res = feature_callback(begin, end);
        sorted_records.insert(pair<int, string>(res,r));
//        sorted_records[res] = r;
    }

    for (auto itr = sorted_records.begin(); itr != sorted_records.end(); ++itr)
        fprintf(f, "%s\n", itr->second.c_str());
}
