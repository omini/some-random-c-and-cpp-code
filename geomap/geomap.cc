#include "geomap.h"
#include <stddef.h>
#include <vector>

using namespace std;

class Point {
public:
    double x;
    double y;
    const char * descr;

    Point() : x(), y(), descr() {};

    Point(const double x, const double y, const char * descr) : x(x), y(y), descr(descr) {};

    Point(const Point& p) : x(p.x), y(p.y), descr(p.descr) {};

    Point & operator = (const Point p) {
        x = p.x;
        y = p.y;
        descr = p.descr;
        return *this;
    }

    ~Point() {};
};

class Rectangle {
public:
    double left_x;
    double bottom_y;
    double right_x;
    double top_y;
    const char * descr;

    Rectangle() : left_x(), bottom_y(), right_x(), top_y(), descr() {};

    Rectangle(const double left_x, const double bottom_y, const double right_x, const double top_y, const char *descr )
        :  left_x(left_x), bottom_y(bottom_y), right_x(right_x), top_y(top_y), descr(descr) {};

    Rectangle(const Rectangle& r)
        : left_x(r.left_x), bottom_y(r.bottom_y), right_x(r.right_x), top_y(r.top_y), descr(r.descr) {};

    Rectangle & operator = (const Rectangle& r) {
        left_x = r.left_x;
        bottom_y = r.bottom_y;
        right_x = r.right_x;
        top_y = r.top_y;
        descr = r.descr;
        return *this;
    }



    ~Rectangle() {};
};


struct geomap {
    vector<Point> points;
    vector<Rectangle> rectangles;
};

/* Create and return a new geomap object.  Return 0 on failure. */
struct geomap * geomap_new() {
    struct geomap * m;
    m = new geomap;
    return m;
}

/* Destroy the given geomap object and release all memory allocated by it. */
void geomap_delete(struct geomap * m) {
    delete(m);
}

void geomap_clear(struct geomap * m) {
    m->points.clear();
    m->rectangles.clear();
}

/* Add a point with the given coordinates and description.
 * Return 0 on failure, 1 on success. */
int geomap_add_point(struct geomap * m, double x, double y, const char * descr) {
    Point p;
    p = Point(x, y, descr);
    m->points.push_back(p);
    return 1;
}

/* Add a rectangle with the given coordinates and description.
 * Return 0 on failure, 1 on success */
int geomap_add_rectangle(struct geomap * m,
                         double left_x, double bottom_y, double right_x, double top_y,
                         const char * descr) {
    Rectangle r;
    r = Rectangle(left_x, bottom_y, right_x, top_y, descr);
    m->rectangles.push_back(r);
    return 1;
}

/* Remove a point with the given coordinates and description.
 * Return 0 if not found, 1 if found (and removed). */
int geomap_remove_point(struct geomap * m, double x, double y, const char * descr) {
    for (auto itr = m->points.begin(); itr != m->points.end(); ++itr) {
        if (itr->x == x && itr->y == y && itr->descr == descr) {
            m->points.erase(itr);
            return 1;
        }
    }
    return 0;
}

/* Remove a rectangle with the given coordinates and description.
 * Return 0 if not found, 1 if found (and removed). */
int geomap_remove_rectangle(struct geomap * m,
                            double left_x, double bottom_y, double right_x, double top_y,
                            const char * descr) {
    for (auto itr = m->rectangles.begin(); itr != m->rectangles.end(); ++itr) {
        if (itr->left_x == left_x && itr->bottom_y == bottom_y && itr->right_x == right_x && itr->top_y == top_y
                && itr->descr == descr) {
            m->rectangles.erase(itr);
            return 1;
        }
    }
    return 0;
}


/* Iterate over all the elements in the geomap.
 * If the point callback is not null, calls that function with each point.
 * If the rectangle callback is not null, calls that function with each rectangle.
 * Return the number of elements in the iteration. */
size_t geomap_iterate_all(struct geomap * m,
                          point_callback pcb, rectangle_callback rcb) {
    size_t size = 0;
    for (auto itr = m->points.begin(); itr != m->points.end(); ++itr) {
        ++size;
        if (pcb) {
            pcb(itr->x, itr->y, itr->descr);
        }
    }
    for (auto itr = m->rectangles.begin(); itr != m->rectangles.end(); ++itr) {
        ++size;
        if (rcb) {
            rcb(itr->left_x, itr->bottom_y, itr->right_x, itr->top_y, itr->descr);
        }
    }

    return size;
}

/* Iterate over the element in the geomap that appear in a given region.
 * If the point callback is not null, calls that function with each point.
 * If the rectangle callback is not null, calls that function with each rectangle.
 * Return the number of elements in the iteration. */
size_t geomap_iterate_in_region(struct geomap * m,
                                point_callback pcb, rectangle_callback rcb,
                                double left_x, double bottom_y, double right_x, double top_y) {
    size_t size = 0;

    for (auto itr = m->points.begin(); itr != m->points.end(); ++itr) {
        if (itr->x >= left_x && itr->x <= right_x && itr->y >= bottom_y && itr->y <= top_y) {
            ++size;
            if (pcb) {
                pcb(itr->x, itr->y, itr->descr);
            }
        }
    }

    for (auto itr = m->rectangles.begin(); itr != m->rectangles.end(); ++itr) {
        if ((itr->left_x >= left_x && itr->left_x <= right_x && itr->top_y >= bottom_y && itr->top_y <= top_y) ||
                (itr->left_x >= left_x && itr->left_x <= right_x && itr->bottom_y >= bottom_y && itr->bottom_y <= top_y) ||
                (itr->right_x >= left_x && itr->right_x <= right_x && itr->top_y >= bottom_y && itr->top_y <= top_y) ||
                (itr->right_x >= left_x && itr->right_x <= right_x && itr->bottom_y >= bottom_y && itr->bottom_y <= top_y))
        {
            ++size;
            if (rcb) {
                rcb(itr->left_x, itr->bottom_y, itr->right_x, itr->top_y, itr->descr);
            }
        }
    }

    return size;
}

