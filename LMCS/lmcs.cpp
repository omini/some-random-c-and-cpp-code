#include <iostream>
#include <vector>
#include <string>

using namespace std;

int max(int a, int b) {
    return (a > b) ? a : b;
}

int lmcs(string a, string b) {
    int Board[a.size() + 1][b.size() + 1];
    int i, j;

    for (i = 0; i <= a.size() ; ++i) {
        for (j = 0; j <= b.size(); ++j) {
            if (i == 0 || j == 0)
                Board[i][j] = 0;

            else if (a.at(i - 1) == b.at(j - 1)) {
                Board[i][j] = Board[i-1][j-1] + 1;
            }

            else
                Board[i][j] = max(Board[i-1][j], Board[i][j-1]);
        }
    }
    return Board[a.size()][b.size()];
}

bool compare(pair<string, int> & a, pair<string, int> & b) {
    return a.second > b.second;
}

int main(int argc, char const **argv) {
    if (argc < 2) {
        cout << "Usage: " << argv[0] << " word" << endl;
        exit(1);

    }
    string arg1(argv[1]);

    vector< pair<string, int> > valid_arg;
    valid_arg.emplace_back("clone", 0);
    valid_arg.emplace_back("init", 0);
    valid_arg.emplace_back("add", 0);
    valid_arg.emplace_back("mv", 0);
    valid_arg.emplace_back("restore", 0);
    valid_arg.emplace_back("rm", 0);
    valid_arg.emplace_back("bisect", 0);
    valid_arg.emplace_back("diff", 0);
    valid_arg.emplace_back("grep", 0);
    valid_arg.emplace_back("log", 0);
    valid_arg.emplace_back("show", 0);
    valid_arg.emplace_back("status", 0);
    valid_arg.emplace_back("branch", 0);
    valid_arg.emplace_back("commit", 0);
    valid_arg.emplace_back("merge", 0);
    valid_arg.emplace_back("rebase", 0);
    valid_arg.emplace_back("reset", 0);
    valid_arg.emplace_back("switch", 0);
    valid_arg.emplace_back("tag", 0);
    valid_arg.emplace_back("fetch", 0);
    valid_arg.emplace_back("pull", 0);
    valid_arg.emplace_back("push", 0);

    for (auto& v : valid_arg) {
        v.second = lmcs(arg1, v.first);
    }

    sort(valid_arg.begin(), valid_arg.end(), compare);


    cout << argv[0] << ": '" << argv[1] << "' is not a valid argument." << endl;
    if (arg1 == valid_arg[0].first || valid_arg[0].second < (valid_arg[0].second / 2) -1 ) {
        return 0;
    }


    cout << endl << "The most similar argument are" << endl;
    int max = 0;
    for (auto& v : valid_arg) {
        if (max > v.second)
            break;
        cout << "\t\t" << v.first << endl;
        max = v.second;
    }

    return 0;
}
