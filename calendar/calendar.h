#ifndef __CALENDAR_H__
#define __CALENDAR_H__

#include <string>
#include <vector>

using std::vector;
using std::string;

class AbstractCalendar {
public:

    virtual void addAppointment(const string & begin, const string & end) = 0;

    virtual void clear() = 0;

    virtual int neighborCountForCity(const string & city) const = 0;

    virtual int countAppointments() const = 0;

    /* Destructor
     */
    virtual ~AbstractCalendar() {};
};

class AbstractConstraint {
public:
    virtual void addConstraint(const string & begin, const string & end) = 0;

    virtual void clear() = 0;

    virtual ~AbstractConstraint() {};
};

extern AbstractCalendar * createCalendar();
extern AbstractConstraint * createConstraint();

#endif // _CALENDAR_H__
