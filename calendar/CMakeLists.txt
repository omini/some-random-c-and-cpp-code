cmake_minimum_required(VERSION 3.15)
project(calendar)

set(CMAKE_CXX_STANDARD 11)

add_executable(calendar calendar.cpp)