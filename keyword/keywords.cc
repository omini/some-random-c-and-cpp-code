#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include <fstream>
#include <set>
#include <cmath>


using namespace std;

bool sort_ascending (const pair<string, pair<double, double> > &a, const pair<string,pair<double, double> > &b) {
    if (a.second.first == b.second.first)
        return lexicographical_compare(a.first.begin(), a.first.end(), b.first.begin(), b.first.end());
    return (a.second.first < b.second.first);
}

bool sort_descending (const pair<string, pair<double, double> > &a, const pair<string, pair<double, double> > &b) {
    if (a.second.first == b.second.first)
        return lexicographical_compare(a.first.begin(), a.first.end(), b.first.begin(), b.first.end());
    return (a.second.first > b.second.first);
}

int main (int argc, char* argv[]) {
    bool (*comparator)(const pair<string, pair<double, double>  > &, const pair<string, pair<double, double> > &);
    comparator = &sort_descending;
    bool fancy = 0;
    double l = 10;
    double h = 90;
    map<string, double> F;
    string name = "";
    vector<string> filenames;

    bool after_args = 0;
    for (int i = 1; i < argc ; ++i) {

        if (string(argv[i]) == "-f" && !after_args)
            fancy = 1;
        else if (string(argv[i]) == "-r" && !after_args)
            comparator = &sort_ascending;
        else if (string(argv[i]).substr(0, 4) == "low=" && !after_args) {
            int size = string(argv[i]).size() - 4;
            l = stoul(string(argv[i]).substr(4, size));
        }
        else if (string(argv[i]).substr(0, 5) == "high=" && !after_args) {
            int size = string(argv[i]).size() - 5;
            h = stoul(string(argv[i]).substr(5, size));
        }
        else if (string(argv[i]) == "--") {
            after_args = 1;
            name = argv[i+1];
            bool inserted = 0;
            for (auto& filename : filenames)
                if (filename == name)
                    inserted = 1;
            if (!inserted)
                filenames.push_back(name);

        }
        else {
            after_args = 1;
            name = argv[i];
            bool inserted = 0;
            for (auto& filename : filenames)
                if (filename == name)
                    inserted = 1;
            if (!inserted)
                filenames.push_back(name);
        }

//    cout << h << " : " << l << endl;
#if 0
        else {
            cerr << "Usage: " << argv[0] << " <low=l> <high=h> <fancy -f> <inverts order -r> <-- filename>" << endl;
            exit(1);
        }
#endif

    }

    string str;
    int counter = 0;
    if (filenames.empty()) { // read from stdin
        while (cin >> str) {
            string tmp;
            for (auto& c : str) {
                if (isalpha(c)) {
                    tmp.push_back(c);
                } else {
                    if (!tmp.empty()) {
                        F[tmp] += 1;
                        counter +=1;
                        tmp.clear();
                    }
                }
            }
            if (!tmp.empty()) {
                F[tmp] +=1;
                ++counter;
                tmp.clear();
            }
        }

        // map to vector to sort elements based on the value and not the key
        vector<pair<string, pair<double, double> > > vec;
        for (auto& word : F) {
            double freq = (word.second / counter) * 100;
            vec.push_back(make_pair(word.first, make_pair(freq, word.second)));
        }

        // sorting vector
        sort(vec.begin(), vec.end(), comparator);

        //printing information
        for (auto& word : vec) {
            if (word.second.first >= l && word.second.first <= h) {
                if (fancy) {
                    cout << " " << word.second.second << ":" << word.first;
                }
                else {
                    cout << " " << word.first;
                }
            }
        }

        cout << endl;

    } else { // read from file
        for (auto& filename : filenames) {
            ifstream inFile;
            inFile.open(filename);
            counter = 0;
            F.clear();
            if (!inFile.is_open()) {
                cerr << "Could not open " << filename << endl;
                exit(1);
            }

            while (inFile >> str) {
                string tmp;
                for (auto& c : str) {
                    if (isalpha(c)) {
                        tmp.push_back(c);
                    } else {
                        if (!tmp.empty()) {
                            F[tmp] += 1;
                            counter +=1;
                            tmp.clear();
                        }
                    }
                }
                if (!tmp.empty()) {
                    F[tmp] +=1;
                    ++counter;
                    tmp.clear();
                }
            }

            // map to vector to sort elements based on the value and not the key
            vector<pair<string, pair<double, double> > > vec;
            vec.clear();
            for (auto& word : F) {
                double freq = (word.second / counter) * 100;
                vec.push_back(make_pair(word.first, make_pair(freq, word.second)));
            }

            // sorting vector
            sort(vec.begin(), vec.end(), comparator);

            //printing information
            cout << filename;
            for (auto& word : vec) {
                if (floor(word.second.first) >= l && floor(word.second.first) <= h) {
                    if (fancy) {
                        cout << " " << word.second.second << ":" << word.first;
                    }
                    else {
                        cout << " " << word.first;
                    }
                }
            }

            cout << endl;
        }

    }
}
