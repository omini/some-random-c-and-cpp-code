
#include <iostream>
#include <string>
#include <cstdlib>
#include <set>
#include <map>
#include <string>
#include <cctype>

using namespace std;

int main () {
    string tag;
    string interests("@mamma #ciao miao #bellaciao");
    set<string> tags;
//    cout << endl << interests << endl;
    bool begin = true;
    for (string::iterator c = interests.begin(); c != interests.end(); ++c) {

        if ( isalpha(*c) || (begin && (*c == '#' || *c == '@'))) {
            tag.push_back(*c);
//            cout << tag << endl;
            begin = false;
        } else {
            if (!tag.empty()) {
                tags.insert(tag);
                tag.clear();
            }
            begin = true;
        }
    }
    if (!tag.empty()) {
        tags.insert(tag);
    }

    for (set<string>::iterator itr = tags.begin(); itr != tags.end(); ++itr) {
        cout << *itr << " ";
    }

    cout << endl;
}