#include <set>
#include <iostream>
#include <string>

int main () {
    std::set<std::string> records;
    records.insert(std::string("carrots 10KG CHF 20"));
    std::string tmp;
    for (auto record : records) {
        tmp = record;
        records.erase(tmp); // seg fault as iteration loose last element
    }

//    records.erase(std::string(std::string("carrots 10KG CHF 20")));


    std::cout << records.size() << std::endl;
}
