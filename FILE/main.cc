#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <set>

using namespace std;

int main(int argc, char const * argv[]) {
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <extensions> <file>"  << endl;
        exit(1);
    }

    set<string> extension;
    const char * ext_filename = argv[1];
    const char * doc_filename = argv[2];
    ifstream extFile(ext_filename, ios::binary);
    if (!extFile.is_open()) {
        cerr << "failed to open " << ext_filename << endl;
        exit(1);
    }

    ifstream docFile(doc_filename, ios::binary);
    if (!docFile.is_open()) {
        cerr << "failed to open " << doc_filename << endl;
        exit(1);
    }

    string line;

    while(getline(extFile, line)) {
        istringstream input_line(line);
        extension.insert(line);

    }
#if 0
    set<string>::iterator itr = extension.begin();
    for (; itr != extension.end(); ++itr )
        cout << *itr << endl;
#endif

    // need to read line by line and split all the string after the first point
    while(getline(docFile, line)) {
        if (line.empty())
            continue;

        istringstream input_line(line);
        string::iterator itr = line.begin();
        for (; *itr != '.'; ++itr);

        string tmp = "";
        ++itr;

        for (; itr != line.end(); ++itr) {
            tmp.push_back(*itr);
        }
#if 0
        for (set<string>::iterator itr = extension.begin(); itr != extension.end(); ++itr) {
            if (tmp == *itr)
                cout << line << endl;
        }
#else
        for (auto& ext : extension ) {
            if (tmp == ext)
                cout << line << endl;
        }
#endif
    }

}