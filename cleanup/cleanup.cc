#include <iostream>
#include <set>
#include <map>
#include <fstream>
#include <sstream>
#include <dirent.h>

using namespace std;

int main(int argc, char const * argv[]) {
    set<string> directories;

    for (int i = 1; i < argc; ++i)
        directories.insert(string(argv[i]));

    map<string, set<string> > rules;
    ifstream rulesFile("RULES");
    if (!rulesFile) {
        cerr << "failed to open RULES" << endl;
        exit(1);
    }

    string ext;
    while (rulesFile >> ext) {
        string src_ext;
        string der_ext;
        string::iterator ch = ext.begin();
        for (; *ch != '.'; ++ch ) {
            src_ext.push_back(*ch);
        }

        for (; ch != ext.end(); ++ch) {
            if (*ch != '.')
                der_ext.push_back(*ch);
            else {
                if (!der_ext.empty()) {
                    rules[src_ext].insert(der_ext);
                    der_ext.clear();
                }
            }
        }
        if (!der_ext.empty()) {
            rules[src_ext].insert(der_ext);
            der_ext.clear();
        }
        src_ext.clear();
    }
#if 0
    for (auto r : rules ) {
        cout << "key: " << r.first << "\t values: ";
        for (auto d : r.second)
            cout << d << " ";
        cout << endl;
    }
#endif

    for (auto& dir : directories) {
        const char& ch = dir.front();
        DIR * dirp = opendir(&ch);
        if (!dirp) {
            cerr << "Could not open directory " << &ch << ", or cannot malloc enough memory" <<endl;
            exit(1);
        }
        set<string> files;
        struct dirent * DirEntry;
        while ((DirEntry = readdir(dirp)) != NULL) {
            files.insert(string(DirEntry->d_name));
        }
        closedir(dirp);
#if 0
        cout << endl << endl;
        for (auto f : files)
            cout << f << endl;
        cout << endl << endl;
#endif
        set<string> print;
        for (auto& key : rules) {
            for (auto& f : files) {
                if ( f.size() >= key.first.size() && (f.compare(f.size() - key.first.size(), key.first.size(), key.first) == 0)) {
                    for (auto& der_ext : key.second) {
                        for (auto& der : files) {
                            int counter = 0;
                            auto itr = f.begin();
                            while (*itr != '.') {
                                ++counter;
                                ++itr;
                            }
                            if (der.compare(0, counter, f, 0, counter) != 0)
                                continue;

                            if ( der.size() >= der_ext.size() && (der.compare(der.size() - der_ext.size(), der_ext.size(), der_ext)) == 0) {
                                print.insert(der);
                            }
                        }
                    }
                }

            }
        }

        for (auto& p : print)
            cout << dir << "/" << p << endl;

        files.clear();
        print.clear();
    }

}